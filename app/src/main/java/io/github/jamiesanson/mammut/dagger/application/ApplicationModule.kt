package io.github.jamiesanson.mammut.dagger.application

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.github.jamiesanson.mammut.data.database.MammutDatabase
import io.github.jamiesanson.mammut.feature.themes.ThemeEngine
import io.github.jamiesanson.mammut.data.repo.PreferencesRepository

@Module(includes = [ ApplicationViewModelModule::class ])
class ApplicationModule(private val appContext: Context) {


    @Provides
    @ApplicationScope
    fun provideApplicationContext(): Context = appContext

    @Provides
    @ApplicationScope
    fun providePreferencesRepository(context: Context): PreferencesRepository =
            PreferencesRepository(context)

    @Provides
    @ApplicationScope
    fun provideThemeEngine(preferencesRepository: PreferencesRepository): ThemeEngine =
            ThemeEngine(preferencesRepository)

    @Provides
    @ApplicationScope
    fun provideMammutDatabase(context: Context): MammutDatabase =
            Room.databaseBuilder(context, MammutDatabase::class.java, "mammut-db").build()
}