package io.github.jamiesanson.mammut.data.models

data class Tag(
        val tagName: String = "",
        val tagUrl: String = ""
)