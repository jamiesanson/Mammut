package io.github.jamiesanson.mammut.feature.instance.subfeature.navigation

/**
 * Listener root fragments should implement to handle reselection events
 */
interface ReselectListener {

    fun onTabReselected()
}