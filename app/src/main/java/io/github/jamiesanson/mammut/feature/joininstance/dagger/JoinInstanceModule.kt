package io.github.jamiesanson.mammut.feature.joininstance.dagger

import dagger.Module

@Module(includes = [ JoinInstanceViewModelModule::class ])
object JoinInstanceModule