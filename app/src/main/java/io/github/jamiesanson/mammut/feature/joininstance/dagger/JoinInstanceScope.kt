package io.github.jamiesanson.mammut.feature.joininstance.dagger

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class JoinInstanceScope