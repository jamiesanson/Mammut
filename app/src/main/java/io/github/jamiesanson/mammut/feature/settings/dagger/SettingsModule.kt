package io.github.jamiesanson.mammut.feature.settings.dagger

import dagger.Module


@Module(includes = [SettingsViewModelModule::class])
class SettingsModule {

}
