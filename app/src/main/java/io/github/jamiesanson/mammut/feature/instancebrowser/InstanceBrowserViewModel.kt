package io.github.jamiesanson.mammut.feature.instancebrowser

import androidx.lifecycle.ViewModel

/**
 * ViewModel for the instance browser activity
 */
class InstanceBrowserViewModel: ViewModel() {

}