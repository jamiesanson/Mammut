package io.github.jamiesanson.mammut.feature.instance.subfeature

import android.widget.ImageView

interface FullScreenPhotoHandler {

    fun displayFullScreenPhoto(imageView: ImageView, photoUrl: String)
}