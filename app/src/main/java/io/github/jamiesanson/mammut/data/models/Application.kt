package io.github.jamiesanson.mammut.data.models

data class Application(
        val appName: String = "",

        val website: String? = null)
