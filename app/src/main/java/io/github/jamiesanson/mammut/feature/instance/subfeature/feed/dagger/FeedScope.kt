package io.github.jamiesanson.mammut.feature.instance.subfeature.feed.dagger

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class FeedScope