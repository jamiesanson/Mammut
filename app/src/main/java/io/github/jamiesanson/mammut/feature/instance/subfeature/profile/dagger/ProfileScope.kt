package io.github.jamiesanson.mammut.feature.instance.subfeature.profile.dagger

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ProfileScope