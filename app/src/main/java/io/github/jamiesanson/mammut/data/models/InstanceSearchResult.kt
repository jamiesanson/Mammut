package io.github.jamiesanson.mammut.data.models

data class InstanceSearchResult(
        val name: String,
        val users: Long
)