package io.github.jamiesanson.mammut.data.models

data class Mention(
        val mentionUrl: String = "",
        val mentionUsername: String = "",
        val mentionAcct: String = "",
        val mentionId: Long = 0)