package io.github.jamiesanson.mammut.feature.instance.dagger

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class InstanceScope