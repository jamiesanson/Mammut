package io.github.jamiesanson.mammut.feature.settings.dagger

import javax.inject.Scope

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class SettingsScope