# Mammut
An Android Client for Mastodon. 

This project is under heavy development. You can find it in beta on the Play Store [here](https://play.google.com/store/apps/details?id=io.github.jamiesanson.mammut).
